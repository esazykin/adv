<?php

include_once 'inc.php';

$outerHosts = ['http://vk.com/', 'http://ya.ru/', 'http://yahoo.com/',];
$innerHosts = ['http://domain.ru/', 'http://domain.com/',];
$browsers = ['ie', 'ff', 'chrome', 'opera'];
$os = ['mac', 'lin', 'win'];

$clients = array_map(function ($i) use ($browsers, $os) {
    return [
        'ip' => long2ip(mt_rand()),
        'browser' => rand_elem($browsers),
        'os' => rand_elem($os),
    ];
}, range(0, 500));
put2file('2.txt', $clients);

$hits = [];
foreach ($clients as $client) {
    $datetime = new DateTime();
    $transitions = mt_rand(3, 6);
    $urlFrom = rand_elem($outerHosts) . rand_str();
    for ($i = 0; $i < $transitions; $i++) {
        $datetime->add(new DateInterval('PT10S'));
        $urlTo = rand_elem($innerHosts) . rand_str();
        $hits[] = [
            'date' => $datetime->format('Y-m-d'),
            'time' => $datetime->format('h:i:s'),
            'ip' => $client['ip'],
            'url_from' => $urlFrom,
            'url_to' => $urlTo,
        ];
        $urlFrom = $urlTo;
    }
}
put2file('1.txt', $hits);


function rand_str()
{
    return substr(md5(rand()), 0, 7);
}

function rand_elem(array $ar)
{
    return $ar[array_rand($ar)];
}

function put2file($filename, array $ar)
{
    file_put_contents($filename, implode(PHP_EOL, array_map(function ($v) {
        return implode(SEP, $v);
    }, $ar)));
}