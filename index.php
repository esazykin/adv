<?php

include_once 'inc.php';


// all of one query
$r = $db->query("

SELECT
  min(concat(s.date, ' ', s.time)) as first_time,
  s1.dt as last_time,
  s.ip_address, s.browser, s.os, s.url_from as first_url_from,
  (
    SELECT s3.url_to
    FROM stat AS s3
    WHERE s3.ip_address = s.ip_address
    ORDER BY s3.date DESC, s3.time DESC
    LIMIT 1
  ) as last_url_to,
  s3.unique_count,
  TIMESTAMPDIFF(SECOND , min(concat(s.date, ' ', s.time)), s1.dt) as timespend
FROM stat as s
JOIN (
       select max(concat(date, ' ', time)) as dt, ip_address
         FROM stat
         GROUP BY ip_address
       ) as s1 ON s1.ip_address = s.ip_address

JOIN (
      select ip_address, count(DISTINCT(url_to)) AS unique_count
      from stat
      GROUP BY ip_address
    ) as s3 ON s3.ip_address = s.ip_address
GROUP BY s.ip_address
ORDER BY s.date ASC, s.time ASC

");

if (!$r->rowCount()) {
    exit('Empty');
}
?>

<table>
    <tr>
        <th>ip</th>
        <th>browser</th>
        <th>os</th>
        <th>first_url_from</th>
        <th>last_url_to</th>
        <th>unique_count</th>
        <th>timespend (seconds)</th>
    </tr>
    <?php foreach ($r as $row) { ?>
        <tr>
            <td><?= $row['ip_address'] ?></td>
            <td><?= $row['browser'] ?></td>
            <td><?= $row['os'] ?></td>
            <td><?= $row['first_url_from'] ?></td>
            <td><?= $row['last_url_to'] ?></td>
            <td><?= $row['unique_count'] ?></td>
            <td><?= $row['timespend'] ?></td>
        </tr>
    <?php } ?>
</table>
