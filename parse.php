<?php

include_once 'inc.php';

$clients = [];
$file = fopen("2.txt", "r");
while (!feof($file)) {
    $line = get_line($file);
    $clients[$line[0]] = [
        'ip_address' => $line[0],
        'browser' => $line[1],
        'os' => $line[2],
    ];
}
fclose($file);

$result = [];
$file = fopen("1.txt", "r");
while (!feof($file)) {
    $line = get_line($file);
    if (!isset($clients[$line[2]])) {
        continue;
    }
    $result[] = $clients[$line[2]] + [
        'date' => $line[0],
        'time' => $line[1],
        'url_from' => $line[3],
        'url_to' => $line[4],
    ];
}
fclose($file);

$dataToInsert = array();

foreach ($result as $data) {
    foreach($data as $val) {
        $dataToInsert[] = $val;
    }
}
$colNames = ['ip_address', 'browser', 'os', '`date`', '`time`', 'url_from', 'url_to'];
$rowPlaces = '(' . implode(', ', array_fill(0, count($colNames), '?')) . ')';
$allPlaces = implode(', ', array_fill(0, count($result), $rowPlaces));

try {
    $stmt = $db->prepare("INSERT INTO stat (" . implode(',', $colNames) . ") VALUES $allPlaces");
    $stmt->execute($dataToInsert);
} catch (PDOException $e){
    echo $e->getMessage();
}

function get_line($file) {
    return array_map('trim', explode(SEP, fgets($file)));
}

